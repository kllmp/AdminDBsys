﻿using System;
using System.Collections.Generic;
using System.Configuration;

using MSSQL.Module;


namespace Soptec.AdminDB.Core
{
    public class BIServidores
    {
        private string GetVersionServer(string ip, string usuario, string pwd)
        {
            string version = String.Empty;
            var db = new MSSQL_Connection();
            try
            {
                db.ConnectionString = string.Format("Data Source={0};Persist Security Info=True;User ID={1};Password={2};Integrated Security=False", ip, usuario, pwd);
                db.Open();
                db.Exec("SELECT SERVERPROPERTY ('ProductVersion') As [Version]");

                if (db.StatusError)
                {
                    
                    Helpers.DebugTest.FileWrite(
                        "[" +
                        DateTime.Now.ToString("yyyy-MM-dd H:mm:ss tt") +
                        "] - Class: BIServidores -Method: GetVersionServer -Message Exception Query: " +
                        db.MessageError
                    );
                }

                if(db.Fetch())
                {
                    version = (string)db.Record[0];
                }
                
            }
            catch(Exception err)
            {
                Helpers.DebugTest.FileWrite(
                    "[" +
                    DateTime.Now.ToString("yyyy-MM-dd H:mm:ss tt") +
                    "] - Class: BIServidores -Method: GetVersionServer -Message Exception: " +
                    err.Message 
                );
            }
            finally
            {
                db.Close();
            }
            
            return version;
        }
        public List<Models.ServidoresModelo> GetServidores(int opcion, string ip)
        {
            List<Models.ServidoresModelo> servidores = new List<Models.ServidoresModelo>();
            var db = new MSSQL_Connection();
            try
            {
                db.ConnectionString = ConfigurationManager.ConnectionStrings["servidor"].ConnectionString;
                db.Open();


                db.Exec("dbo.spCnCatServidores", new List<System.Data.SqlClient.SqlParameter>
                {
                    new System.Data.SqlClient.SqlParameter("@Opcion", (object)opcion),
                    new System.Data.SqlClient.SqlParameter("@IP", (object)ip),
                });

                if(db.StatusError)
                {
                    Helpers.DebugTest.FileWrite(
                       "[" +
                       DateTime.Now.ToString("yyyy-MM-dd H:mm:ss tt") +
                       "] - Class: BIServidores -Method: GetServidores -Error Query: " +
                       db.MessageError
                   );
                }

                if (opcion == 0)
                {
                    while (db.Fetch())
                    {
                        servidores.Add(new Models.ServidoresModelo
                        {
                            Id = (int)db.Record[0],
                            Nombre = (string)db.Record[1],
                            Alias = (string)db.Record[2],
                            IP = (string)db.Record[3],
                            UsuarioDB = (string)db.Record[4],
                            ContraseniaDB = (string)db.Record[5],
                            Version = GetVersionServer((string)db.Record[3], (string)db.Record[4], (string)db.Record[5]),
                        });
                    }
                }
                else if(opcion == 1)
                {
                    if (db.Fetch())
                    {
                        servidores.Add(new Models.ServidoresModelo
                        {
                            Id = (int)db.Record[0],
                            Nombre = (string)db.Record[1],
                            Alias = (string)db.Record[2],
                            IP = (string)db.Record[3],
                            UsuarioDB = (string)db.Record[4],
                            ContraseniaDB = (string)db.Record[5],
                            Version = GetVersionServer((string)db.Record[3], (string)db.Record[4], (string)db.Record[5]),
                        });
                    }
                }


            }
            catch(Exception err)
            {
                Helpers.DebugTest.FileWrite(
                    "[" +
                    DateTime.Now.ToString("yyyy-MM-dd H:mm:ss tt") +
                    "] - Class: BIServidores -Method: GetServidores -Message Exception: " +
                    err.Message +
                    " -Origin" +
                    err.Source
                );
            }
            finally
            {
                db.Close();
            }

            return servidores;
            
        }


        private List<Models.VistasModelo> GetVistas(string connectionString)
        {
            List<Models.VistasModelo> vistas = new List<Models.VistasModelo>();
            var db = new MSSQL_Connection();
            try
            {
                db.ConnectionString = connectionString;
                db.Open();

                db.Exec("SELECT [TABLE_SCHEMA] + '.' +  [TABLE_NAME] As Nombre FROM [INFORMATION_SCHEMA].[VIEWS]");
                if (db.StatusError)
                {
                    Helpers.DebugTest.FileWrite(
                       "[" +
                       DateTime.Now.ToString("yyyy-MM-dd H:mm:ss tt") +
                       "] - Class: BIServidores -Method: GetVistas(string connectionString) -Exception Query: " +
                       db.MessageError
                   );
                }

                while (db.Fetch())
                {
                    vistas.Add(new Models.VistasModelo
                    {
                        Nombre = (string)db.Record[0]
                    });
                }

            }
            catch (Exception err)
            {
                Helpers.DebugTest.FileWrite(
                   "[" +
                   DateTime.Now.ToString("yyyy-MM-dd H:mm:ss tt") +
                   "] - Class: BIServidores -Method: GetVistas(string connectionString) -Message Exception: " +
                   err.Message
               );
            }
            finally
            {
                db.Close();
            }

            return vistas;
        }
        private List<Models.FuncionesModelo> GetFunciones(string connectionString)
        {
            List<Models.FuncionesModelo> funciones = new List<Models.FuncionesModelo>();
            var db = new MSSQL_Connection();
            try
            {
                db.ConnectionString = connectionString;
                db.Open();

                db.Exec("SELECT [SPECIFIC_SCHEMA] + '.' + [ROUTINE_NAME] As Nombre FROM INFORMATION_SCHEMA.ROUTINES WHERE [ROUTINE_TYPE] = 'FUNCTION'");
                if (db.StatusError)
                {
                    Helpers.DebugTest.FileWrite(
                       "[" +
                       DateTime.Now.ToString("yyyy-MM-dd H:mm:ss tt") +
                       "] - Class: BIServidores -Method: GetFunciones(string connectionString) -Exception Query: " +
                       db.MessageError
                   );
                }

                while (db.Fetch())
                {
                    funciones.Add(new Models.FuncionesModelo
                    {
                        Nombre = (string)db.Record[0]
                    });
                }

            }
            catch (Exception err)
            {
                Helpers.DebugTest.FileWrite(
                   "[" +
                   DateTime.Now.ToString("yyyy-MM-dd H:mm:ss tt") +
                   "] - Class: BIServidores -Method: GetFunciones(string connectionString) -Message Exception: " +
                   err.Message
               );
            }
            finally
            {
                db.Close();
            }

            return funciones;
        }
        private List<Models.ProcedimientosModelo> GetProcedimientos(string connectionString)
        {
            List<Models.ProcedimientosModelo> procedimientos = new List<Models.ProcedimientosModelo>();
            var db = new MSSQL_Connection();
            try
            {
                db.ConnectionString = connectionString;
                db.Open();

                db.Exec("SELECT [SPECIFIC_SCHEMA] + '.' + [ROUTINE_NAME] As Nombre FROM INFORMATION_SCHEMA.ROUTINES WHERE [ROUTINE_TYPE] = 'PROCEDURE'");
                if(db.StatusError)
                {
                    Helpers.DebugTest.FileWrite(
                       "[" +
                       DateTime.Now.ToString("yyyy-MM-dd H:mm:ss tt") +
                       "] - Class: BIServidores -Method: GetProcedimientos(string connectionString) -Exception Query: " +
                       db.MessageError
                   );
                }

                while(db.Fetch())
                {
                    procedimientos.Add(new Models.ProcedimientosModelo
                    {
                        Nombre = (string)db.Record[0]
                    });
                }

            }
            catch (Exception err)
            {
                Helpers.DebugTest.FileWrite(
                   "[" +
                   DateTime.Now.ToString("yyyy-MM-dd H:mm:ss tt") +
                   "] - Class: BIServidores -Method: GetProcedimientos(string connectionString) -Message Exception: " +
                   err.Message
               );
            }
            finally
            {
                db.Close();
            }

            return procedimientos;
        }
        private List<Models.TablaModelo> GetTablas(string connectionString)
        {
            List<Models.TablaModelo> tablas = new List<Models.TablaModelo>();
            var db = new MSSQL_Connection();
            try
            {
                db.ConnectionString = connectionString;
                db.Open();

                db.Exec("SELECT [TABLE_SCHEMA]  + '.' + [TABLE_NAME] As Nombre FROM [INFORMATION_SCHEMA].[TABLES]");

                if(db.StatusError)
                {
                    Helpers.DebugTest.FileWrite(
                       "[" +
                       DateTime.Now.ToString("yyyy-MM-dd H:mm:ss tt") +
                       "] - Class: BIServidores -Method: GetTablas(string database) -Exception Query: " +
                       db.MessageError
                   );
                }

                while(db.Fetch())
                {
                    tablas.Add(new Models.TablaModelo {
                        Nombre = (string)db.Record[0]
                    });
                }
            }
            catch(Exception err)
            {
                Helpers.DebugTest.FileWrite(
                   "[" +
                   DateTime.Now.ToString("yyyy-MM-dd H:mm:ss tt") +
                   "] - Class: BIServidores -Method: GetTablas(string database) -Message Exception: " +
                   err.Message
               );
            }
            finally
            {
                db.Close();
            }

            return tablas;
        }




        public List<Models.DataBaseModelo> GetDataBases(string ip)
        {
            List<Models.DataBaseModelo> dataBases = new List<Models.DataBaseModelo>();
            var db = new MSSQL_Connection();
            try
            {
                
                var servidorores = GetServidores(1, ip);

                foreach (var servidor in servidorores)
                {
                    db.ConnectionString = ConfigurationManager.ConnectionStrings["servidor"].ConnectionString;
                    db.Open();

                    db.Exec("SELECT [name] As Nombre FROM [sys].[databases]  where[name] Not In('master', 'tempdb', 'model', 'msdb')");
                    if (db.StatusError)
                    {
                        Helpers.DebugTest.FileWrite(
                           "[" +
                           DateTime.Now.ToString("yyyy-MM-dd H:mm:ss tt") +
                           "] - Class: BIServidores -Method: GetDataBases(string ip) -Message Exception Query: " +
                           db.MessageError
                       );
                    }

                    while (db.Fetch())
                    {
                        string strConexion = string.Format("Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3};Integrated Security=False", servidor.IP, (string)db.Record[0], servidor.UsuarioDB, servidor.ContraseniaDB);
                        dataBases.Add(new Models.DataBaseModelo
                        {
                            IP = servidor.IP,
                            Nombre = (string)db.Record[0],
                            Tablas = GetTablas(strConexion),
                            Vistas = GetVistas(strConexion),
                            Funciones = GetFunciones(strConexion),
                            Procedimientos = GetProcedimientos(strConexion),
                        });
                    }

                    db.Close();
                }

            }
            catch(Exception err)
            {
                Helpers.DebugTest.FileWrite(
                   "[" +
                   DateTime.Now.ToString("yyyy-MM-dd H:mm:ss tt") +
                   "] - Class: BIServidores -Method: GetDataBases(string ip) -Message Exception: " +
                   err.Message
               );
            }
            finally
            {
                db.Close();
            }

            return dataBases;
        }
    }
}