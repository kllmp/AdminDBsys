﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Soptec.AdminDB.Models
{
    public class ServidoresModelo
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Alias { get; set; }
        public string IP { get; set; }
        public string UsuarioDB { get; set; }
        public string ContraseniaDB { get; set; }
        public string Version { get; set; }
        //public string ConnectionString { get; set; }
    }
}