﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Soptec.AdminDB.Models
{
    public class DataBaseModelo
    {
        public string IP { get; set; }
        public string Nombre { get; set; }
        public List<TablaModelo> Tablas { get; set; }
        public List<ProcedimientosModelo> Procedimientos { get; set; }
        public List<FuncionesModelo> Funciones { get; set; }
        public List<VistasModelo> Vistas { get; set; }


    }
}