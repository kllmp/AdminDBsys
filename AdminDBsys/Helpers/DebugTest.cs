﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Configuration;
using System.IO;

namespace Soptec.AdminDB.Helpers
{
    public static class DebugTest
    {
        public static void FileWrite(string text)
        {
            try
            {
                string pathFile = ConfigurationManager.AppSettings["pathLogFile"].ToString();

                if (!Directory.Exists(pathFile))
                    Directory.CreateDirectory(pathFile);

                pathFile += DateTime.Now.ToString("yyyy-MM-dd") + ".log";

                using (StreamWriter archivo = new StreamWriter(pathFile, true))
                {
                    archivo.WriteLine(text);
                    archivo.Close();
                }
            }
            catch(Exception )
            {
                //Console.WriteLine(err);
            }
        }
    }
}