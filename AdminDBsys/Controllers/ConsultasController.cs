﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Soptec.AdminDB.Controllers
{
    public class ConsultasController : Controller
    {
        private Core.BIServidores BIserv;
        public ConsultasController()
        {
            BIserv = new Core.BIServidores();
        }


        [HttpPost]
        public JsonResult GetServidores()
        {
            try
            {
                var obj = new { Datos = BIserv.GetServidores(0,String.Empty), Status = true, Mensaje = "" };
                var resp = Json(obj);
                resp.MaxJsonLength = 999999999;
                return resp;
            }
            catch(Exception err)
            {
                var obj = new { Datos = "", Status = false, Mensaje = err.Message };
                return Json(obj);
            }
        }

        [HttpPost]
        public JsonResult GetDataBase(string ip)
        {
            try
            {
                var obj = new { DataBases = BIserv.GetDataBases(ip), Status = true, Mensaje = "" };
                var resp = Json(obj);
                resp.MaxJsonLength = 999999999;
                return resp;
            }
            catch (Exception err)
            {
                var obj = new { DataBases = "", Status = false, Mensaje = err.Message };
                return Json(obj);
            }
        }
    }
}