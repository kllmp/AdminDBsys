﻿var JTreeViewsElemento = (function () {
    function AddNodeChildren(lista) {
        var nodes = [];
        for (let i in lista) {
            var NodeChildren = {
                'text': lista[i].Nombre, 
                "icon": "jstree-file"
            }
            nodes.push(NodeChildren);
        }
        return nodes;
    }
    var TreeViewsElemento = function () {
        var self = this;


        self.GetDataTreeView = function (nombreServ, lista) {
            var servidor = {
                'text': nombreServ, 'children': [], "state": { "opened": true },
            };

            for (let i in lista) {
                var Node = {
                    "text": lista[i].Nombre,
                    "children": [
                        { "text": 'Tablas', "children": []},
                        { "text": 'Procedimientos', "children": []},
                        { "text": 'Funciones', "children": []},
                        { "text": 'Vistas', "children": []},
                    ],
                }


                Node.children[0].children = AddNodeChildren(lista[i].Tablas)
                Node.children[1].children = AddNodeChildren(lista[i].Procedimientos)
                Node.children[2].children = AddNodeChildren(lista[i].Funciones)
                Node.children[3].children = AddNodeChildren(lista[i].Vistas)

                
                servidor.children.push(Node);
            }

            return servidor;
        }
    }

    return new TreeViewsElemento();
})();